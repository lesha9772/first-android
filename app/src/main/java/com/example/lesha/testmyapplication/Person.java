package com.example.lesha.testmyapplication;

/**
 * Created by alexeypolishchuk on 5/8/17.
 */

public class Person {
    String name;
    String age;
    int photoId;

    public Person(String name, String age, int photoId) {
        this.name = name;
        this.age = age;
        this.photoId = photoId;
    }
}