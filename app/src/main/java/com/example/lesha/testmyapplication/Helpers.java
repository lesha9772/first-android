package com.example.lesha.testmyapplication;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;



/**
 * Created by alex on 11/19/16.
 */

public class Helpers {
    static Context context;
    static JSONObject responseRequest;






    static public JsonObjectRequest sendPost(Map<String, String> jsonParams, String url){
        Log.d("MYLOG","JsonObjectRequest");
        JsonObjectRequest myRequest = new JsonObjectRequest(
                Request.Method.GET,
                url,
                new JSONObject(jsonParams),

                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
//                        verificationSuccess(response);
                        Log.d("MYLOG",response.toString());
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                        verificationFailed(error);
                        Log.d("MYLOG",error.toString());

                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
//                headers.put("User-agent", "android client");
                return headers;
            }
        };


        return myRequest;
    }

    static public JsonObjectRequest requestGet(Map<String, String> jsonParams, String url){
        Log.d("MYLOG","JsonObjectRequest requestGet");
        JsonObjectRequest myRequest = new JsonObjectRequest(
                Request.Method.GET,
                url,
                new JSONObject(jsonParams),

                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("MYLOG",response.toString());
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                        verificationFailed(error);
                        Log.d("MYLOG",error.toString());

                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
//                headers.put("User-agent", "android client");
                return headers;
            }
        };


        return myRequest;
    }


}
