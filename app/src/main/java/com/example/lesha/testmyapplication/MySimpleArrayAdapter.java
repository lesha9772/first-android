package com.example.lesha.testmyapplication;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;


/**
 * Created by alexeypolishchuk on 5/9/17.
 */

public class MySimpleArrayAdapter extends BaseAdapter {
    private final Context context;
    View rowView;
    ImageView imageView;
    TextView email, username;
    ArrayList<UserItem> items;
    MemoryCache memoryCache = new MemoryCache();
    private ProgressDialog simpleWaitDialog;

    public MySimpleArrayAdapter(Context context, ArrayList<UserItem> items) {
//        super(context, -1);
        this.context = context;
        this.items = items;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        rowView = inflater.inflate(R.layout.rowlayout, parent, false);
        username = (TextView) rowView.findViewById(R.id.username);
        email = (TextView) rowView.findViewById(R.id.email);
        imageView = (ImageView) rowView.findViewById(R.id.icon);
        UserItem userItem = items.get(position);

        username.setText(userItem.username);
        email.setText(userItem.email);

        Bitmap bitmap = memoryCache.get(userItem.image);
        if (bitmap != null)
            imageView.setImageBitmap(bitmap);
        else {
            new ImageDownloader().execute(userItem.image);
        }

        return rowView;
    }


    private class ImageDownloader extends AsyncTask<String, Object, Bitmap> {
        protected Bitmap doInBackground(String... urls) {
            return downloadBitmap(urls[0]);
        }

        protected void onProgressUpdate(Object... progress) {
//            simpleWaitDialog = ProgressDialog.show(context,
//                    "Wait", "Downloading Image");
        }

        protected void onPostExecute(Bitmap result) {
            imageView.setImageBitmap(result);
//            simpleWaitDialog.dismiss();
        }

        private Bitmap downloadBitmap(String url) {

            try {
                URL image_url = new URL(url);
                URLConnection conn = image_url.openConnection();
                final Bitmap bitmap = BitmapFactory.decodeStream(conn.getInputStream());
                if (bitmap != null) {
                    memoryCache.put(url, bitmap);
                }
                return bitmap;
            } catch (Exception e) {
                Log.e("ImageDownloader", "Something went wrong while" +
                        " retrieving bitmap from " + url + e.toString());

            }

            return null;
        }
    }


    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int arg0) {
        return items.get(arg0);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
}
