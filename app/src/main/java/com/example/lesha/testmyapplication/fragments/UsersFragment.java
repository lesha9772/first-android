package com.example.lesha.testmyapplication.fragments;


import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.lesha.testmyapplication.UserItem;
import com.example.lesha.testmyapplication.MySimpleArrayAdapter;
import com.example.lesha.testmyapplication.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

/**
 * Created by alex on 11/19/16.
 */

public class UsersFragment extends ListFragment {
    private final String url = "http://print-it.tk/api/v1/users"; // return json users
    String data[] = new String[]{"one", "two", "three", "four"};
    String countries;
    Context context;
    StringBuilder sb = new StringBuilder();

    ArrayList list = new ArrayList();
    ArrayList<UserItem> usersList = new ArrayList<UserItem>();
    MyTask mt;
    TextView count_users;
    ProgressBar progressBar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mt = new MyTask();
        mt.execute();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


//        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
//                android.R.layout.simple_list_item_1, values);
//        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.rowlayout, R.id.label, values);
//        setListAdapter(adapter);


//        ArrayList<UserItem> stooges = new ArrayList<UserItem>();
//        for (int i = 1; i <= 5; i++) {
//            stooges.add(new UserItem("Product ", R.drawable.agenda));
//        }

//        MySimpleArrayAdapter adapter = new MySimpleArrayAdapter(getActivity(), stooges);
//        setListAdapter(adapter);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_users,
                container, false);


        count_users = (TextView) view.findViewById(R.id.count_users);
        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        progressBar.setVisibility(View.VISIBLE);


        return view;
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
//        Toast.makeText(getActivity(), "position = " + position, Toast.LENGTH_SHORT).show();
        String item = (String) getListAdapter().getItem(position);
        Toast.makeText(getActivity(), item + " selected", Toast.LENGTH_LONG).show();
    }


    private void getUsers() {
        RequestQueue rq = Volley.newRequestQueue(getActivity().getApplicationContext());
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Do something with the response
                        try {

                            JSONArray values = new JSONArray(response);
                            for (int i = 0; i < values.length(); i++) {

                                Log.d("MYLOG", values.getJSONObject(i).toString());
                                JSONObject sonuc = values.getJSONObject(i);
                                usersList.add(new UserItem(sonuc.getString("username"),sonuc.getString("email"), sonuc.getString("img")));


                            }

                        } catch (JSONException ex) {

                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Handle error
                    }
                });

        rq.add(stringRequest);
    }


    class MyTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            getUsers();
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                TimeUnit.SECONDS.sleep(2);


            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            MySimpleArrayAdapter adapter = new MySimpleArrayAdapter(getActivity(), usersList);
            setListAdapter(adapter);
            count_users.setText("Количество пользователей: " + usersList.size());
            progressBar.setVisibility(View.INVISIBLE);
        }
    }


/*


    private void getCountries() {
        RequestQueue rq = Volley.newRequestQueue(getActivity().getApplicationContext());
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Do something with the response
                        Log.d("MYLOG", response.toString());
                        try{

                            JSONArray values = new JSONArray(response);
//                            JSONArray values=o.getJSONArray("response");

                            for ( int i=0; i< values.length(); i++) {


                                Log.d("MYLOG", values.getJSONObject(i).toString());
                                JSONObject sonuc = values.getJSONObject(i);
                                list.add(sonuc.getString("name"));
                                sb.append("id: " + sonuc.getString("id") + "\n");
                                sb.append("name: " + sonuc.getString("name") + "\n\n");
                            }

//                            Log.d("MYLOG", sb.toString());


                        }  catch (JSONException ex){

                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Handle error
                    }
                });

        rq.add(stringRequest );
        }



    private void getCountries2() {

        Map<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("format", "json");

        JsonObjectRequest myRequest = new JsonObjectRequest(
                Request.Method.GET,
                "https://api.meetup.com/2/cities",
                new JSONObject(jsonParams),

                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        countries = response.toString();
                        Log.d("MYLOG", response.toString());

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
//                headers.put("User-agent", "My useragent");
                return headers;
            }
        };

        // Access the RequestQueue through your singleton class.

    }

*/
}
