package com.example.lesha.testmyapplication;

/**
 * Created by alexeypolishchuk on 5/9/17.
 */

public class UserItem {
    String username;
    String email;
    String image;

    public UserItem(String _username,   String _email, String _image) {
        username = _username;
        email = _email;
        image = _image;
    }
}
